// indexedDB: Reforzamiento

let request = window.indexedDB.open('mi-database', 1);


//se actualiza cuando se crea o sube la version de la base de datos

request.onupgradeneeded = event => {
    console.log("actualizacion de la BD");

    let db = event.target.result;

    db.createObjectStore('heroes', {
        keyPath: 'id'
    });
}

//manejo de  errores
request.onerror = event => {
    console.log("error DB", event.target.error);
}

//insertar datos
request.onsuccess = event => {

    let db = event.target.result;
    let heroesData = [
        { id: '111', heroe: 'spiderman', mensaje: 'Aquí su amigo spiderman' },
        { id: '222', heroe: 'Iroman', mensaje: 'Aquí con mi nuevo mark 50' }
    ];

    let heroesTransaction = db.transaction('heroes', 'readwrite');

    heroesTransaction.onerror = event => {
        console.log("Transaccion error", event.target.error);
    }

    heroesTransaction.oncomplete = event => {
        console.log("transaccion completa");
    }

    let heroesStore = heroesTransaction.objectStore('heroes');


    for (let heroe of heroesData) {
        heroesStore.add(heroe);
    }

    heroesStore.onsuccess = event => {
        console.log("nuevo evento agregado al storage");
    }
}